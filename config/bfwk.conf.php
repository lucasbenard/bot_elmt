<?php

/**
 * BFWK_MODE
 * BFwk behaviour
 * - 0 = development : bfwk tries to generate missing code to help
 *       programmers and designers and display PHP errors/warning/notices
 * - 1 = testing : bfwk shows missing code and display PHP errors
 * - 2 = production : security mode for production environment
 */
define('BFWK_MODE',				0);

/**
 * BFWK_SERVER_ROOT
 * root path for current installation
 */
if (isset($_SERVER['HTTPS']) && ! empty($_SERVER['HTTPS']))
	define('BFWK_SERVER_ROOT',	'https://'.$_SERVER['HTTP_HOST'].'');
else
	define('BFWK_SERVER_ROOT',	'http://'.$_SERVER['HTTP_HOST'].'');

/**
 * BFWK_DEFAULT_CTRL
 * controller used if HTTP query string is empty
 */
define('BFWK_DEFAULT_CTRL',		'main');

/**
 * BFWK_DEFAULT_VIEW
 * view used if HTTP query string contains only a controller name
 */
define('BFWK_DEFAULT_VIEW',		'index');

/**
 * BFWK_CTRL_PATH
 * relative or absolute path to controllers directory
 */
define('BFWK_CTRL_PATH',		$_SERVER['DOCUMENT_ROOT'].'/ctrls/');

/**
 * BFWK_VIEW_PATH
 * relative or absolute path to views directory
 */
define('BFWK_VIEW_PATH',		$_SERVER['DOCUMENT_ROOT'].'/views/');

/**
 * BFWK_CLASS_PATH
 * relative or absolute path to classes directory
 */
define('BFWK_CLASS_PATH',		$_SERVER['DOCUMENT_ROOT'].'/classes/');

/**
 * BFWK_PHP_EXT
 * PHP file extension (added to controller and view name)
 * could be '.php', '.phtml', '.inc'...
 */
define('BFWK_PHP_EXT',			'.php');

/**
 * BFWK_DEBUG_VAR
 * name of variable to enter debug mode on view which has not a controller action yet
 * this variable is added by developper at the end of url :
 * http://www.domain.tld/ctrl/view/param&debug
 * This mode permits to display php variable and code used by a view page.
 */
define('BFWK_DEBUG_VAR',		'debug');

/**
 * BFWK_DEFAULT_LANG
 * ISO 3166 code of default langage.
 * HTTP_ACCEPT_LANGUAGE is use first to find preferred lang in lang folder
 */
define('BFWK_DEFAULT_LANG',		'en');
