<?php

/**
 * Use this file to define your constants
 */

date_default_timezone_set('UTC'); // keep it to UTC

define('APP_DB_DSN', 'mysql:host=localhost;dbname=YOUR_DATABASE_NAME;charset=UTF8');
define('APP_DB_USR', 'YOUR_DATABASE_USER');
define('APP_DB_PWD', 'YOUR_DATABASE_PASSWORD');
// Your mysql/mariadb server must have been loaded with Timezone data
// mysql_tzinfo_to_sql /usr/share/zoneinfo | mysql -u root mysql


define('APP_SITE_URL', BFWK_SERVER_ROOT);
define('APP_SITE_NAME', 'Your Bot web site');

define('APP_EMAIL', 'your@mail.tld');
define('APP_EMAIL_FROM', 'no-reply@mail.tld');

define('APP_UPLOADS_PATH', $_SERVER['DOCUMENT_ROOT'].'/uploads/');
define('APP_UPLOADS_LOGS', APP_UPLOADS_PATH . "logs/");
define('APP_UPLOADS_DATA', APP_UPLOADS_PATH . "data/");


define('APP_CRON_TOKEN', 'GENERATE_A_TOKEN_USABLE_IN_AN_URL'); // md5sum a random file for example

// Mastodon
define('APP_MASTODON_INSTANCE_URL', 'https://your.mastodon.instance');
define('APP_MASTODON_MAX_TOOT_LEN', 500);
define('APP_MASTODON_APPNAME', 'Your Bot Name');
define('APP_MASTODON_USER_PROFIL', 'your_masto_name');
define('APP_MASTODON_USER_EMAIL', 'yourmasto@mail.tld');
define('APP_MASTODON_USER_PASSWORD', 'mastopassword');
// open this url to determine following client parameters
// http://yourapp.tld/?/cron/get_mastodon_client_params&crontoken=APP_CRON_TOKEN
define('APP_MASTODON_CLIENT_ID', '');
define('APP_MASTODON_CLIENT_SECRET', '');
define('APP_MASTODON_ACCESS_TOKEN', '');
define('APP_MASTODON_TOKEN_TYPE', 'bearer');

// Nb sec a forecast is valid
define('APP_FORECAST_VALIDITY', 3600);

// OpenWeatherMap
define('APP_OWM_API_APPID', 'your openweathermap appid'); // https://openweathermap.org/appid
define('APP_OWM_CITIES_URL', 'http://bulk.openweathermap.org/sample/city.list.json.gz');
define('APP_OWM_FORECAST_URL', 'http://api.openweathermap.org/data/2.5/forecast');

// Available site langages
$GLOBALS['langs'] = array('en','fr'); // see content of lang/ directory
