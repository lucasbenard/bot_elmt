/* correct HTTP_REFERER for IE */
function goTo(url) {
	
	if(window.unloadfunction)
	{
		if(!unloadfunction())
			return false;
	}
	
	var a = document.createElement("a");
	if(!a.click) { //only IE has this (at the moment);
		window.location = url;
		return;
	}
	a.setAttribute("href", url);
	a.style.display = "none";
	$("body").append(a); //prototype shortcut
	a.click();
}

